<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   error_log(print_r('loADING styles', true));
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
   wp_enqueue_style( 'child-style', get_stylesheet_directory_uri().'/style.css', ['parent-style','flexi-style']  );
   wp_enqueue_style( 'flexi-style', get_stylesheet_directory_uri().'/flexslider.css' );
   error_log(print_r( get_template_directory_uri(), true));

}
add_action( 'wp_enqueue_scripts', 'enqueue_parent_scripts' );
function enqueue_parent_scripts() {
   error_log(print_r('loADING scripts', true));
   wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/bootstrap.min.js' );
   wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() .'/bootstrap.min.css' );

   wp_enqueue_script( 'flexi-script', get_stylesheet_directory_uri().'/flexslider.js', ['jquery'] );
   wp_enqueue_script( 'template-script', get_stylesheet_directory_uri().'/template.js', ['flexi-script'] );
   

} ?>
  